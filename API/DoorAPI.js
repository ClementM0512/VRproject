import axios from 'axios';

export function getDoors(){
    const url = 'http://51.103.16.116/api/doors'
    return new Promise((resolve, reject) => {
        return axios.get(url).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
}

