import { createAppContainer } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Acceuil from '../Components/Acceuil';
import DoorItem from '../Components/DoorItem';
import DoorEdit from '../Components/DoorEdit';

const VRPStackNavigator = createStackNavigator({
    Acceuil:{
        screen: Acceuil,
        navigationOptions: {
            title: 'The door',
            headerStyle:{
                backgroundColor: "#33CFFF",
            },
            headerTitleStyle:{
                textAlign: "center",
            }
        },  
    },
    DoorEdit:{
        screen: DoorEdit,
        navigationOptions: {
            title: 'The door',
            headerStyle:{
                backgroundColor: "#33CFFF",
            },
            headerTitleStyle:{
                textAlign: "center",
            }
        },  
    },
})
export default createAppContainer(VRPStackNavigator)