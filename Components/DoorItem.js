import React from "react";
import { View, Text, StyleSheet, Image, Button } from "react-native";

class DoorItem extends React.Component {
  constructor(props) {
    super(props);
  }

  DoorStatus() {
    const Door = this.props.Door;
    if ((Door.status = true)) {
      return <Text style={styles.DoorEtat}>Ouvert</Text>;
    } else {
      return <Text style={styles.DoorEtat}>Ferme</Text>;
    }
  }

  render() {
    const Door = this.props.Door;
    return (
      <View style={styles.container}>
        <Image style={styles.image} source={require("../assets/TheDoor.png")} />
        <View>
          <Text style={styles.DoorTitre}>{Door.name}</Text>
        </View>
        <View style={styles.Btn}>
          <Button title="Select" onPress={() => this.props.AffDoor()} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  Btn: {
    marginLeft: 100,
    marginTop: 20,
  },
  DoorTitre: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 20,
  },
  DoorEtat: {
    textAlign: "center",
    fontSize: 16,
  },
  image: {
    width: 75,
    height: 75,
    margin: 5,
    backgroundColor: "#D5D0D6",
    borderColor: "black",
    borderRadius: 10,
    borderWidth: 1,
    marginRight: 15,
    marginBottom: 10,
  },
  Door: {
    flexDirection: "row",
  },
});
export default DoorItem;
