import React from "react";
import { View, Text, StyleSheet, Image, Button, FlatList } from "react-native";
import DoorItem from "./DoorItem";
import { getDoors } from "../API/DoorAPI";

class Acceuil extends React.Component {
  constructor(props) {
    super(props);
    this.state = { _Doors: [] };
  }

  componentDidMount() {
    getDoors()
      .then((res) => {
        this.setState({ _Doors: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  AffDoor = () => {
    this.props.navigation.navigate("DoorEdit");
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state._Doors}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <DoorItem Door={item} AffDoor={this.AffDoor}></DoorItem>
          )}
        ></FlatList>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Btn: {
    marginLeft: 100,
    marginTop: 20,
  },
  DoorTitre: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 20,
  },
  DoorEtat: {
    textAlign: "center",
    fontSize: 16,
  },
  image: {
    width: 75,
    height: 75,
    margin: 5,
    backgroundColor: "#D5D0D6",
    borderColor: "black",
    borderRadius: 10,
    borderWidth: 1,
    marginRight: 15,
    marginBottom: 10,
  },
  Door: {
    flexDirection: "row",
  },
  Doors: {},
  Menu: {
    marginTop: 15,
    alignItems: "baseline",
    justifyContent: "flex-start",
    flex: 9,
  },
  container: {
    flex: 1,
  },
});
export default Acceuil;
